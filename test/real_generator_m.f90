module real_generator_m
    use veggies, only: &
            real_input_t, &
            generated_t, &
            generator_t, &
            input_t, &
            shrink_result_t, &
            get_random_real_with_magnitude, &
            shrunk_value, &
            simplest_value

    implicit none
    private
    public :: real_GENERATOR

    type, extends(generator_t) :: real_generator_t
    contains
        private
        procedure, public :: generate
        procedure, public, nopass :: shrink
    end type

    type(real_generator_t) :: real_GENERATOR = &
            real_generator_t()
contains
    function generate(self) result(random_double)
        class(real_generator_t), intent(in) :: self
        type(generated_t) :: random_double

        associate(a => self)
        end associate

        random_double = generated_t(real_input_t( &
                get_random_real_with_magnitude(1.0e6)))
    end function

    function shrink(input) result(shrunk)
        class(input_t), intent(in) :: input
        type(shrink_result_t) :: shrunk

        select type (input)
        type is (real_input_t)
            associate(input_val => input%input())
                if (effectively_zero(input_val)) then
                    shrunk = simplest_value(real_input_t(0.0))
                else
                    shrunk = shrunk_value(real_input_t( &
                            input_val / 2.0))
                end if
            end associate
        end select
    end function

    pure function effectively_zero(value_)
        real, intent(in) :: value_
        logical :: effectively_zero

        effectively_zero = abs(value_) < epsilon(value_)
    end function
end module
