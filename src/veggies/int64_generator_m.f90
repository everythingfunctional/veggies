module veggies_int64_generator_m
    use, intrinsic :: iso_fortran_env, only: int64
    use veggies_generated_m, only: generated_t
    use veggies_generator_m, only: generator_t
    use veggies_input_m, only: input_t
    use veggies_int64_input_m, only: int64_input_t
    use veggies_random_m, only: get_random_int64
    use veggies_shrink_result_m, only: &
            shrink_result_t, shrunk_value, simplest_value

    implicit none
    private
    public :: INT64_GENERATOR

    type, extends(generator_t) :: int64_generator_t
    contains
        private
        procedure, public :: generate
        procedure, nopass, public :: shrink
    end type

    type(int64_generator_t), parameter :: &
            INT64_GENERATOR = int64_generator_t()
contains
    function generate(self) result(generated_value)
        class(int64_generator_t), intent(in) :: self
        type(generated_t) :: generated_value

        associate(unused => self)
        end associate

        generated_value = generated_t(int64_input_t(get_random_int64()))
    end function

    function shrink(input) result(shrunk)
        class(input_t), intent(in) :: input
        type(shrink_result_t) :: shrunk

        select type (input)
        type is (int64_input_t)
            associate(input_val => input%input())
                if (input_val == 0) then
                    shrunk = simplest_value(int64_input_t(0_int64))
                else
                    shrunk = shrunk_value(int64_input_t(input_val / 2))
                end if
            end associate
        end select
    end function
end module
