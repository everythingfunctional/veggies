module veggies_utilities_m
    use, intrinsic :: iso_fortran_env, only: int32, int64
    use iso_varying_string, only: varying_string, operator(//)
    use strff, only: add_hanging_indentation, join, strff_to_string => to_string, NEWLINE

    implicit none
    private
    public :: &
            to_string, &
            equals_within_absolute, &
            equals_within_relative, &
            all_image_reports_i, &
            any_image_failed_i, &
            current_image, &
            num_imgs, &
            all_image_reports, &
            any_image_failed

    interface to_string
        module procedure real_array_to_string
        module procedure real_matrix_to_string
        module procedure real_tensor_to_string
        module procedure double_array_to_string
        module procedure double_matrix_to_string
        module procedure double_tensor_to_string
        module procedure integer_32_array_to_string
        module procedure integer_32_matrix_to_string
        module procedure integer_32_tensor_to_string
        module procedure integer_64_array_to_string
        module procedure integer_64_matrix_to_string
        module procedure integer_64_tensor_to_string
    end interface

    interface equals_within_absolute
        module procedure equals_within_absolute_single
        module procedure equals_within_absolute_double
    end interface

    interface equals_within_relative
        module procedure equals_within_relative_single
        module procedure equals_within_relative_double
    end interface

    abstract interface
        function all_image_reports_i(current_image_report) result(combined)
            import :: varying_string
            type(varying_string), intent(in) :: current_image_report
            type(varying_string) :: combined
        end function

        function any_image_failed_i(current_image_failed)
            logical, intent(in) :: current_image_failed
            logical :: any_image_failed_i
        end function
    end interface

    integer :: current_image = 1
    integer :: num_imgs = 1
    procedure(all_image_reports_i), pointer :: all_image_reports => veggies_all_image_reports
    procedure(any_image_failed_i), pointer :: any_image_failed => veggies_any_image_failed
contains
    pure function real_array_to_string(array) result(string)
        real, intent(in) :: array(:)
        type(varying_string) :: string

        string = "[" // join(strff_to_string(array), ", ") // "]"
    end function

    pure function real_matrix_to_string(matrix) result(string)
        real, intent(in) :: matrix(:,:)
        type(varying_string) :: string

        integer :: i

        string = add_hanging_indentation( &
            "[" // join([(to_string(matrix(i,:)), i = 1, size(matrix, dim=1))], "," // NEWLINE) // "]", &
            1)
    end function

    pure function real_tensor_to_string(tensor) result(string)
        real, intent(in) :: tensor(:,:,:)
        type(varying_string) :: string

        integer :: i

        string = add_hanging_indentation( &
            "[" // join([(to_string(tensor(i,:,:)), i = 1, size(tensor, dim=1))], "," // NEWLINE) // "]", &
            1)
    end function

    pure function double_array_to_string(array) result(string)
        double precision, intent(in) :: array(:)
        type(varying_string) :: string

        string = "[" // join(strff_to_string(array), ", ") // "]"
    end function

    pure function double_matrix_to_string(matrix) result(string)
        double precision, intent(in) :: matrix(:,:)
        type(varying_string) :: string

        integer :: i

        string = add_hanging_indentation( &
            "[" // join([(to_string(matrix(i,:)), i = 1, size(matrix, dim=1))], "," // NEWLINE) // "]", &
            1)
    end function

    pure function double_tensor_to_string(tensor) result(string)
        double precision, intent(in) :: tensor(:,:,:)
        type(varying_string) :: string

        integer :: i

        string = add_hanging_indentation( &
            "[" // join([(to_string(tensor(i,:,:)), i = 1, size(tensor, dim=1))], "," // NEWLINE) // "]", &
            1)
    end function

    pure function integer_32_array_to_string(array) result(string)
        integer(int32), intent(in) :: array(:)
        type(varying_string) :: string

        string = "[" // join(strff_to_string(array), ", ") // "]"
    end function

    pure function integer_32_matrix_to_string(matrix) result(string)
        integer(int32), intent(in) :: matrix(:,:)
        type(varying_string) :: string

        integer :: i

        string = add_hanging_indentation( &
            "[" // join([(to_string(matrix(i,:)), i = 1, size(matrix, dim=1))], "," // NEWLINE) // "]", &
            1)
    end function

    pure function integer_32_tensor_to_string(tensor) result(string)
        integer(int32), intent(in) :: tensor(:,:,:)
        type(varying_string) :: string

        integer :: i

        string = add_hanging_indentation( &
            "[" // join([(to_string(tensor(i,:,:)), i = 1, size(tensor, dim=1))], "," // NEWLINE) // "]", &
            1)
    end function
    
    pure function integer_64_array_to_string(array) result(string)
        integer(int64), intent(in) :: array(:)
        type(varying_string) :: string

        string = "[" // join(strff_to_string(array), ", ") // "]"
    end function

    pure function integer_64_matrix_to_string(matrix) result(string)
        integer(int64), intent(in) :: matrix(:,:)
        type(varying_string) :: string

        integer :: i

        string = add_hanging_indentation( &
            "[" // join([(to_string(matrix(i,:)), i = 1, size(matrix, dim=1))], "," // NEWLINE) // "]", &
            1)
    end function

    pure function integer_64_tensor_to_string(tensor) result(string)
        integer(int64), intent(in) :: tensor(:,:,:)
        type(varying_string) :: string

        integer :: i

        string = add_hanging_indentation( &
            "[" // join([(to_string(tensor(i,:,:)), i = 1, size(tensor, dim=1))], "," // NEWLINE) // "]", &
            1)
    end function

    elemental function equals_within_absolute_single(expected, actual, tolerance)
        real, intent(in) :: expected
        real, intent(in) :: actual
        real, intent(in) :: tolerance
        logical :: equals_within_absolute_single

        equals_within_absolute_single = abs(expected - actual) <= tolerance
    end function

    elemental function equals_within_absolute_double(expected, actual, tolerance)
        double precision, intent(in) :: expected
        double precision, intent(in) :: actual
        double precision, intent(in) :: tolerance
        logical :: equals_within_absolute_double

        equals_within_absolute_double = abs(expected - actual) <= tolerance
    end function

    elemental function equals_within_relative_single(expected, actual, tolerance)
        real, intent(in) :: expected
        real, intent(in) :: actual
        real, intent(in) :: tolerance
        logical :: equals_within_relative_single

        real, parameter :: MACHINE_TINY = tiny(0.0)

        equals_within_relative_single = &
                (abs(expected) <= MACHINE_TINY .and. abs(actual) <= MACHINE_TINY) &
                .or. (abs(expected - actual) / abs(expected) <= tolerance)
    end function

    elemental function equals_within_relative_double(expected, actual, tolerance)
        double precision, intent(in) :: expected
        double precision, intent(in) :: actual
        double precision, intent(in) :: tolerance
        logical :: equals_within_relative_double

        double precision, parameter :: MACHINE_TINY = tiny(0.0d0)

        equals_within_relative_double = &
                (abs(expected) <= MACHINE_TINY .and. abs(actual) <= MACHINE_TINY) &
                .or. (abs(expected - actual) / abs(expected) <= tolerance)
    end function

    function veggies_all_image_reports(current_image_report)
        type(varying_string), intent(in) :: current_image_report
        type(varying_string) :: veggies_all_image_reports

        veggies_all_image_reports = current_image_report
    end function

    function veggies_any_image_failed(current_image_failed)
        logical, intent(in) :: current_image_failed
        logical :: veggies_any_image_failed

        veggies_any_image_failed = current_image_failed
    end function
end module
