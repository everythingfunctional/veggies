module veggies_run_tests_m
    use iso_fortran_env, only: error_unit, int64, output_unit
    use iso_varying_string, only: varying_string, operator(//), put_line, var_str
    use strff, only: to_string, NEWLINE
    use veggies_command_line_m, only: options_t, get_options, DEBUG
    use veggies_test_item_m, only: filter_item_result_t, test_item_t
    use veggies_test_result_item_m, only: test_result_item_t
    use veggies_utilities_m, only: &
            current_image, num_imgs, all_image_reports, any_image_failed

    implicit none
    private
    public :: run_tests
contains
    function run_tests(tests) result(passed)
        type(test_item_t), intent(in) :: tests
        logical :: passed

        integer(int64) :: clock_rate
        type(varying_string) :: current_image_report, whole_report
        real :: elapsed_time
        integer(int64) :: end_time
        type(filter_item_result_t) :: filtered_tests
        type(options_t) :: options
        type(test_result_item_t) :: results
        integer(int64) :: start_time
        logical :: current_image_failed
        type(test_item_t) :: tests_to_run
        integer :: i

        current_image_failed = .false.

        options = get_options()

        tests_to_run = tests
        do i = 1, size(options%filter_strings)
            filtered_tests = tests_to_run%filter(options%filter_strings(i))
            if (filtered_tests%matched()) then
                tests_to_run = filtered_tests%test()
            else
                if (current_image == 1) &
                        call put_line(error_unit, "No matching tests found")
                passed = .false.
                return
            end if
        end do

        if (current_image == 1) then
            call put_line(output_unit, "Running Tests")
            call put_line(output_unit, "")

            if (.not.options%quiet) then
                call put_line(output_unit, tests_to_run%description())
                call put_line(output_unit, "")
            end if

            call put_line( &
                    output_unit, &
                    "A total of " // to_string(tests_to_run%num_cases()) // " test cases")
            call put_line(output_unit, "")
        end if

        if (DEBUG) call put_line( &
                "Beginning execution of test suite" &
                // merge(" on image " // to_string(current_image), var_str(""), num_imgs > 1))
        call system_clock(start_time, clock_rate)
        results = tests_to_run%run()
        call system_clock(end_time)
        if (DEBUG) call put_line( &
                "Completed execution of test suite." &
                // merge(" on image " // to_string(current_image), var_str(""), num_imgs > 1))
        elapsed_time = real(end_time - start_time) / real(clock_rate)

        current_image_failed = .not.results%passed()
        passed = .not.any_image_failed(current_image_failed)
        current_image_report = &
                merge("On image " // to_string(current_image) // NEWLINE, var_str(""), num_imgs > 1) &
                // merge(var_str("Failed"), var_str("All Passed"), current_image_failed) // NEWLINE &
                // "Took " // to_string(elapsed_time, 6) // " seconds" // NEWLINE // NEWLINE &
                // merge( &
                        merge( &
                                results%verbose_description(options%colorize), &
                                results%failure_description(options%colorize), &
                                options%verbose) // NEWLINE // NEWLINE &
                        // to_string(results%num_failing_cases()) // " of " &
                        // to_string(results%num_cases()) // " cases failed" // NEWLINE &
                        // to_string(results%num_failing_asserts()) // " of " &
                        // to_string(results%num_asserts()) // " assertions failed", &
                        merge(results%verbose_description(options%colorize) // NEWLINE // NEWLINE, var_str(""), options%verbose) &
                        // "A total of " // to_string(results%num_cases()) &
                                // " test cases containing a total of " &
                                // to_string(results%num_asserts()) // " assertions", &
                        current_image_failed) // NEWLINE
        whole_report = all_image_reports(current_image_report)
        if (current_image == 1) call put_line(output_unit, whole_report)
    end function
end module
