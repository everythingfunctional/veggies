module veggies_real_input_m
    use veggies_input_m, only: input_t

    implicit none
    private
    public :: real_input_t

    type, extends(input_t) :: real_input_t
        private
        real :: input_
    contains
        private
        procedure, public :: input
    end type

    interface real_input_t
        module procedure constructor
    end interface
contains
    pure function constructor(input) result(real_input)
        real, intent(in) :: input
        type(real_input_t) :: real_input

        real_input%input_ = input
    end function

    pure function input(self)
        class(real_input_t), intent(in) :: self
        real :: input

        input = self%input_
    end function
end module
