module veggies_int64_input_m
    use, intrinsic :: iso_fortran_env, only: int64
    use veggies_input_m, only: input_t

    implicit none
    private
    public :: int64_input_t

    type, extends(input_t) :: int64_input_t
        private
        integer(int64) :: input_
    contains
        private
        procedure, public :: input
    end type

    interface int64_input_t
        module procedure constructor
    end interface
contains
    pure function constructor(input) result(integer_input)
        integer(int64), intent(in) :: input
        type(int64_input_t) :: integer_input

        integer_input%input_ = input
    end function

    pure function input(self)
        class(int64_input_t), intent(in) :: self
        integer(int64) :: input

        input = self%input_
    end function
end module
